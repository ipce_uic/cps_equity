# Tracking Equity in Chicago Public Schools

[Vote Equity](https://voteequity.org) harnessed the collective wisdom of the community to unearth and prioritize ideas that will best improve equity in Chicago. 

There were 3 main steps to Vote Equity -- Chicagoans and community groups first submitted ideas on how to best build a Chicago that works for all of us, then voted on those ideas, and the top ideas then became part of the the Vote Equity questionnaire where candidates had the opportunity to endorse them.

There were more than 50,000 votes cast on over 180 ideas related to issues like Education, Health and Housing. 

The top 6 ideas were all education ideas.

And Mayor Lightfoot [endorsed](https://www.voteequity.org/mayoralmatchup) all of the top education ideas.

**The aim of this project is to comprehensively and publicly track the Lightfoot administration’s progress implementing the [core demand voiced](https://www.voteequity.org/issues): equitable funding for education in Chicago.** 


